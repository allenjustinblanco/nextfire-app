import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDoXSP-4I15NEEEQne8DA163bndk5_Tw8E",
    authDomain: "nextfire-app-35790.firebaseapp.com",
    projectId: "nextfire-app-35790",
    storageBucket: "nextfire-app-35790.appspot.com",
    messagingSenderId: "522522185867",
    appId: "1:522522185867:web:9f52b346ec1e49225d048e",
    measurementId: "G-DBS7KYGLZC"
}

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

/**`
 * Gets a users/{uid} document with username
 * @param  {string} username
*/
export async function getUserWithUsername(username) {
    const usersRef = firestore.collection('users');
    const query = usersRef.where('username', '==', username).limit(1);
    const userDoc = (await query.get()).docs[0];
    return userDoc;
}

/**`
 * Converts a firestore document to JSON
 * @param  {DocumentSnapshot} doc
 */
export function postToJSON(doc) {
    const data = doc.data();
    return {
        ...data,
        // Gotcha! firestore timestamp NOT serializable to JSON. Must convert to milliseconds
        createdAt: data.createdAt.toMillis(),
        updatedAt: data.updatedAt.toMillis(),
    };
}

export const fromMillis = firebase.firestore.Timestamp.fromMillis;
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const storage = firebase.storage();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;
export const increment = firebase.firestore.FieldValue.increment

// Storage exports
export const STATE_CHANGED = firebase.storage.TaskEvent.STATE_CHANGED;
